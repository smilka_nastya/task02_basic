package com.smilka;

import java.util.Scanner;

/**
 * Fibonacci class.
 */

public class Fibonacci {
    /**
     * constant percents.
     */

    private static final int ONE_HUNDRED_PERCENT = 100;
    private static final int DIFFERENCE = 3;

    /**
     * @param args
     */
    public static void main(String[] args) {
        int interval;
        int sumOfOdd = 0;
        int sumOfEven = 0;
        /**
         *create and generate numbers for first task of task02_basic
         */
        System.out.print("Enter the interval: ");
        Scanner num = new Scanner(System.in);
        interval = num.nextInt();
        System.out.println("The interval is [1;" + interval + "]");
        System.out.println("Odd numbers from start to the end of interval: ");
        for (int i = 0; i <= interval; i++) {
            if ((i % 2) == 1) {
                System.out.print(i + ", ");
                sumOfOdd += i;
            }
        }
        System.out.println(" ");
        System.out.println("Even from end to start: ");
        for (int i = interval; i > 0; i--) {
            if ((i % 2) == 0) {
                System.out.print(i + ", ");
                sumOfEven += i;
            }
        }
        System.out.println(" ");
        System.out.println("Sum of odd numbers is: " + sumOfOdd);
        System.out.println("Sum of even numbers is: " + sumOfEven);
        /**
         * begin second part of task
         */
        int n;
        int f0 = 1;
        int f1 = 1;
        int f2;
        int sum1 = 0;
        int sum2 = 2;
        int theBiggestOddF1 = 0;
        int theBiggestEvenF2 = 0;
        int amountOfEven = 0;
        int amountOfOdd = 2;
        float oddInPerc;
        float evenInPerc;
        Scanner in = new Scanner(System.in);
        /**
         *create and generate Fibonacci numbers
         */
        System.out.print("Enter amount of numbers: ");
        n = in.nextInt();
        System.out.println("Fibonacci numbers are: ");
        System.out.print(f1 + " " + f1 + " ");
        for (int i = 0; i <= (n); i++) {
            f2 = f0 + f1;
            System.out.print(f2 + " ");
            if ((f2 % 2) == 0) {
                sum1 += f2;
                theBiggestEvenF2 = f2;
                amountOfEven++;
            } else if ((f2 % 2) == 1) {
                sum2 += f2;
                theBiggestOddF1 = f2;
                amountOfOdd++;
            }
            f0 = f1;
            f1 = f2;
        }
        System.out.println(" ");
        oddInPerc = (ONE_HUNDRED_PERCENT * amountOfOdd) / (n + DIFFERENCE);
        evenInPerc = (ONE_HUNDRED_PERCENT * amountOfEven) / (n + DIFFERENCE);
        /**
         *print F1 - the biggest odd number and F2 – the biggest even number
         */
        System.out.print("Sum of even numbers  = " + sum1);
        System.out.println(",the biggest even number is: " + theBiggestEvenF2);
        System.out.println(oddInPerc + "% odd numbers of all");
        System.out.print("Sum of odd numbers = " + sum2);
        System.out.println(",the biggest odd number is: " + theBiggestOddF1);
        System.out.println(evenInPerc + "% even numbers of all");
    }

}

